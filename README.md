# Kubernetes Demo

---

 - Configure kubectl

 - Show cluster nodes
```
kubectl get nodes
```

 - Show configuration files under config-k8s\
 
 - Deploy everything under "config-k8s":

```
kubectl create -f .\config-k8s\
```

 - Show deployments and services:

```
kubectl get deployments
kubectl get services
```

 - Browse to the application (vote, result)

 - Scale the vote service (modify configuration file)

``` 
kubectl apply -f .\config-k8s\vote-deployment.yaml
kubectl get deployments
```

 - Create namespace (feature environment) - Show configuration
```
kubectl create -f .\config-k8s-namespace\feature-namespace.yaml
```

 - Create deployments and services in new environment (change services ports + namespace + docker images)
```
kubectl create -f .\config-k8s-updated
```

 - Invite to vote

---

## CLEANUP

```
kubectl delete namespace feature
kubectl delete services --all
kubectl delete deployments --all
```
